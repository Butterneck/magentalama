/* MagentaLama
  Copyright (C) 2019  Filippo Pinton

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function whichTransitionEvent(){
    var t;
    var el = document.createElement('fakeelement');
    var transitions = {
      'transition':'transitionend',
      'OTransition':'oTransitionEnd',
      'MozTransition':'transitionend',
      'WebkitTransition':'webkitTransitionEnd'
    }

    for(t in transitions){
        if( el.style[t] !== undefined ){
            return transitions[t];
        }
    }
}

var transitionEnd = whichTransitionEvent();

const navSlide = () => {
    const burger = document.querySelector('.burger');
    const navMenu = document.querySelector('#nav-menu');
    const navLinks = document.querySelectorAll('#nav-active li');
    var semaphore = 1;
    // When semaphore is set to 1 it means that the click can be
    // registered, although, if semaphore is set to 0 it means that
    // a transition is running and no click could be registered.
    // This is to prevent bad behaviour of the burger menu

    // Toggle nav
    burger.addEventListener('click', () => {
        if (navMenu.classList.contains('nav-hidden')) { // Show menu
            if (semaphore) {    // Check if the transition can happen
                navMenu.classList.remove('nav-hidden');
                navMenu.classList.add('nav-active');
            }
        } else if (semaphore) {    // Hide menu
            semaphore = 0;  // Lock semaphore
            navMenu.classList.remove('nav-active');
            navMenu.classList.add('nav-visually-hidden');
            burger.classList.toggle('toggle');
            navMenu.addEventListener(transitionEnd, function(e) {
                navMenu.classList.remove('nav-visually-hidden');
                navMenu.classList.add('nav-hidden');
                semaphore = 1;  // Unlock semaphore
            }, false);
        }

        // Burger animation
        if (semaphore) {
            burger.classList.toggle('toggle');
        }
    });
}


navSlide();
